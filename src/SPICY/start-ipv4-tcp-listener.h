#ifndef __SPICY_START_IPv4_TCP_LISTENER
#define __SPICY_START_IPv4_TCP_LISTENER

#include <stdint.h> // uint16_t

#include "SPICY/data-types/socket-context.h" // struct SPICY_socket_context

void SPICY_start_ipv4_tcp_listener(char*, uint16_t, void(*)(struct SPICY_socket_context*), void(*)(struct SPICY_socket_context*));

#endif
