#include <stddef.h> // NULL
#include <sys/epoll.h> // epoll stuff
#include <sys/socket.h> // accept()

#include "SPICY/data-types/socket-context.h" // struct SPICY_socket_context

#include "SPICY/threads/add-socket.h" // SPICY_add_socket()

void SPICY_default_listener_event_handler(struct SPICY_socket_context *listener_socket_context){
	int new_socket;

	for(;;){
		new_socket = accept(listener_socket_context->socket, NULL, NULL);
		if(new_socket < 1)
			return;

		SPICY_add_socket(new_socket, (void (*)(struct SPICY_socket_context*))(intptr_t)listener_socket_context->app_data, NULL);
	}
}
