#include <pwd.h> // getpwnam()
#include <stdbool.h> // bool / true / false
#include <stddef.h> // NULL
#include <stdio.h> // sprintf()
#include <stdlib.h> // calloc()
#include <signal.h> // sig*() / sigset_t / SIG_*
#include <unistd.h> // setgid() / setuid()

#include "SPICY/data-types/core-config.h" // struct SPICY_core_config
#include "SPICY/data-types/socket-context.h" // struct SPICY_socket_context

#include "SPICY/threads/setup.h" // SPICY_threads_setup()

struct SPICY_core_config SPICY_core_config = {
	.process_user = NULL,
	.process_group = NULL,
	.error_message = NULL,
	.max_socket_fd = 8192
};

struct SPICY_socket_context *SPICY_socket_contexts_array;


bool SPICY_init(void){
	sigset_t new_mask;
	sigfillset(&new_mask);
	sigdelset(&new_mask, SIGINT); // Allow ctrl + c to terminate the process

	if(sigprocmask(SIG_SETMASK, &new_mask, NULL) < 0){
		SPICY_core_config.error_message = "Failed to set process signal mask";
		return false;
	}



	// Now drop privileges (if provided)
	if(SPICY_core_config.process_user != NULL){
		struct passwd *passwd = getpwnam(SPICY_core_config.process_user);

		if(passwd == NULL){
			sprintf(SPICY_core_config.error_message, "Failed to find user/group \"%s\" when dropping privileges", SPICY_core_config.process_user);
			return false;
		}

		if(SPICY_core_config.process_group != NULL && setgid(passwd->pw_gid) < 0){
			sprintf(SPICY_core_config.error_message, "Failed to drop group privileges to \"%s\"", SPICY_core_config.process_group);
			return false;
		}

		if(setuid(passwd->pw_uid) < 0){
			sprintf(SPICY_core_config.error_message, "Failed to drop user privileges for \"%s\"", SPICY_core_config.process_user);
			return false;
		}
	}



	SPICY_socket_contexts_array = calloc(sizeof(struct SPICY_socket_context), SPICY_core_config.max_socket_fd);

	// Now set up the socket_context array
	for(uint32_t i=0; i<SPICY_core_config.max_socket_fd; i++){
		SPICY_socket_contexts_array[i].socket = i;
		SPICY_socket_contexts_array[i].zeroed[1] = 0;
		SPICY_socket_contexts_array[i].state = SPICY_SOCKET_CONTEXT_STATE_FREE;
	}

	return SPICY_threads_setup();
}
