#ifndef __SPICY_DEFAULT_LISTENER_EVENT_HANDLER
#define __SPICY_DEFAULT_LISTENER_EVENT_HANDLER

#include "SPICY/data-types/socket-context.h" // struct SPICY_socket_context

void SPICY_default_listener_event_handler(struct SPICY_socket_context*);

#endif
