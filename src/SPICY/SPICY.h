#ifndef __SPICY_H
#define __SPICY_H

#include "SPICY/init.h"

#include "SPICY/start-ipv4-tcp-listener.h"
#include "SPICY/default-listener-event-handler.h"

#include "SPICY/data-types/threads-config.h"
#include "SPICY/data-types/socket-context.h"
#include "SPICY/data-types/core-config.h"

#include "SPICY/threads/add-socket.h"

#endif
