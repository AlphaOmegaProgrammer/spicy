#ifndef __SPICY_CORE_CONFIG
#define __SPICY_CORE_CONFIG

#include <stdint.h> // uint32_t

struct SPICY_core_config{
	char *process_user, *process_group, *error_message;
	uint32_t max_socket_fd;
};

#endif
