#ifndef __SPICY_THREADS_CONFIG
#define __SPICY_THREADS_CONFIG

#include <stdint.h> // uint32_t

struct SPICY_threads_config{
	uint32_t thread_pool_size;
};

#endif
