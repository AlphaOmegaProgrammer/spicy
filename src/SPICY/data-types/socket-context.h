#ifndef __SPICY_SOCKET_CONTEXT
#define __SPICY_SOCKET_CONTEXT

#include <stdint.h> // uint32_t

enum SPICY_socket_context_state{
	SPICY_SOCKET_CONTEXT_STATE_PERMANENT,
	SPICY_SOCKET_CONTEXT_STATE_WAITING,
	SPICY_SOCKET_CONTEXT_STATE_PROCESSING,
	SPICY_SOCKET_CONTEXT_STATE_KEEP_PROCESSING,
	SPICY_SOCKET_CONTEXT_STATE_FREE
};

struct SPICY_socket_context{
	// Make the struct 32 bytes long, and make state able to be used easily with cmpxchg8b
	uint32_t socket;
	uint32_t zeroed[2];
	enum SPICY_socket_context_state state;

	void (*event_handler)(struct SPICY_socket_context*);
	void *app_data;
};

#endif
