#include <arpa/inet.h> // inet_addr()
#include <fcntl.h> // F_GETFL / F_SETFL / O_NONBLOCK
#include <netinet/in.h> // struct sockaddr_in and other socket related stuff
#include <stdint.h> // uint16_t`
#include <sys/epoll.h> // epoll stuff

#include "SPICY/data-types/core-config.h" // struct SPICY_core_config
#include "SPICY/data-types/socket-context.h" // struct SPICY_socket_context

extern int SPICY_epoll_fd;
extern struct SPICY_core_config SPICY_core_config;
extern struct SPICY_socket_context *SPICY_socket_contexts_array;

void SPICY_start_ipv4_tcp_listener(char *ipv4_address, uint16_t tcp_port, void(*event_handler)(struct SPICY_socket_context*), void(*client_event_handler)(struct SPICY_socket_context*)){
	struct epoll_event epoll_event = {
		.events = EPOLLIN | EPOLLOUT | EPOLLET
	};

	int listener_socket = socket(AF_INET, SOCK_STREAM, 0);
	if(listener_socket < 0 || (uint32_t)listener_socket >= SPICY_core_config.max_socket_fd)
		return;

	int listener_socket_flags = fcntl(listener_socket, F_GETFL, 0);
	if(listener_socket_flags < 0)
		return;

	if(fcntl(listener_socket, F_SETFL, listener_socket_flags | O_NONBLOCK) < 0)
		return;


	size_t sockaddr_in_size = sizeof(struct sockaddr_in);
	struct sockaddr_in listener_sockaddr_in;
	listener_sockaddr_in.sin_family = AF_INET;
	listener_sockaddr_in.sin_port = htons(tcp_port);
	listener_sockaddr_in.sin_addr.s_addr = inet_addr(ipv4_address);

	if(bind(listener_socket, (struct sockaddr*)&listener_sockaddr_in, sockaddr_in_size) < 0)
		return;

	while(SPICY_socket_contexts_array[listener_socket].state != SPICY_SOCKET_CONTEXT_STATE_FREE);

	SPICY_socket_contexts_array[listener_socket].state = SPICY_SOCKET_CONTEXT_STATE_PERMANENT;

	SPICY_socket_contexts_array[listener_socket].socket = listener_socket;

	SPICY_socket_contexts_array[listener_socket].event_handler = event_handler;
	SPICY_socket_contexts_array[listener_socket].app_data = (void*)(intptr_t)client_event_handler;

	if(listen(listener_socket, 1024) < 0)
		return;

	epoll_event.data.fd = listener_socket;
	epoll_ctl(SPICY_epoll_fd, EPOLL_CTL_ADD, listener_socket, &epoll_event);
}
