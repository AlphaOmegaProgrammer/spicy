#include <fcntl.h> // fcntl()
#include <sys/epoll.h> // epoll stuff

#include "SPICY/data-types/core-config.h" // struct SPICY_core_config
#include "SPICY/data-types/socket-context.h" // struct SPICY_socket_context

extern struct SPICY_core_config SPICY_core_config;
extern struct SPICY_socket_context *SPICY_socket_contexts_array;

extern int SPICY_epoll_fd;

void SPICY_add_socket(int socket_fd, void(*event_handler)(struct SPICY_socket_context*), void* app_data){
	if((int)SPICY_core_config.max_socket_fd <= socket_fd){
		return;
	}

	struct epoll_event epoll_event = {
		.events = EPOLLIN | EPOLLOUT | EPOLLET,
		.data.fd = socket_fd
	};

	SPICY_socket_contexts_array[socket_fd].event_handler = event_handler;
	SPICY_socket_contexts_array[socket_fd].app_data = app_data;
	SPICY_socket_contexts_array[socket_fd].state = SPICY_SOCKET_CONTEXT_STATE_WAITING;

	int socket_fd_flags = fcntl(socket_fd, F_GETFL, 0);
	if(socket_fd_flags < 0)
		return;

	if(fcntl(socket_fd, F_SETFL, socket_fd_flags | O_NONBLOCK) < 0)
		return;

	epoll_ctl(SPICY_epoll_fd, EPOLL_CTL_ADD, socket_fd, &epoll_event);
}
