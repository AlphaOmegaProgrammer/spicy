.section .note.GNU-stack

.section .text


.global SPICY_thread_function
	.type SPICY_thread_function, @function
SPICY_thread_function:
	# 0(%rsp) = %rbx
	# 8(%rsp) = %r12
	# 16(%rsp) = %r13
	# 24(%rsp) = %r14
	# 32(%rsp) = %r15
	# 40(%rsp) = struct epoll_event
	# 52(%rsp) = unused
	sub $64, %rsp

	mov %rbx, 0(%rsp)
	mov %r12, 8(%rsp)
	mov %r13, 16(%rsp)
	mov %r14, 24(%rsp)
	mov %r15, 32(%rsp)

	mov SPICY_epoll_fd@gotpcrel(%rip), %r12
	movslq (%r12), %r12

	mov SPICY_socket_contexts_array@gotpcrel(%rip), %r13
	mov (%r13), %r13

	lea 40(%rsp), %r14

	xor %ebx, %ebx

	SPICY_THREAD_FUNCTION_LOOP:
		# result = epoll_wait(SPICY_epoll_fd, &epoll_event, 1, -1)
		mov $232, %eax
		mov %r12, %rdi
		mov %r14, %rsi
		mov $1, %edx
		mov $-1, %r10
		syscall
		# if(result < 1) goto SPICY_THREAD_FUNCTION_LOOP
		cmp $0, %eax
		jle SPICY_THREAD_FUNCTION_LOOP

		mov 4(%r14), %r15
		shl $2, %r15
		lea (%r13, %r15, 8), %r15

		movl 12(%r15), %edx
		cmp $0, %edx
		je SPICY_THREAD_FUNCTION_CALL_PERMANENT_EVENT_HANDLER

		xor %eax, %eax

		SPICY_THREAD_FUNCTION_ATOMIC_ACQUIRE_LOOP:
			cmp $3, %edx
			jge SPICY_THREAD_FUNCTION_LOOP

			mov %edx, %ecx
			inc %ecx
			lock cmpxchg8b 8(%r15)
		jne SPICY_THREAD_FUNCTION_ATOMIC_ACQUIRE_LOOP

		cmp $3, %ecx
		je SPICY_THREAD_FUNCTION_LOOP

		SPICY_THREAD_FUNCTION_CALL_EVENT_HANDLER:

		mov 16(%r15), %rcx
		mov %r15, %rdi
		call *%rcx

		mov 8(%r15), %edx

		SPICY_THREAD_FUNCTION_ATOMIC_RELEASE_LOOP:
			mov %edx, %ecx
			dec %ecx
			lock cmpxchg8b 8(%r15)
		jne SPICY_THREAD_FUNCTION_ATOMIC_RELEASE_LOOP

		cmp $1, %ecx
		je SPICY_THREAD_FUNCTION_CALL_EVENT_HANDLER
	jmp SPICY_THREAD_FUNCTION_LOOP

	xor %eax, %eax							# return 0

	mov 32(%rsp), %r15
	mov 24(%rsp), %r14
	mov 16(%rsp), %r13
	mov 8(%rsp), %r12
	mov 0(%rsp), %rbx

	add $64, %rsp
	ret

# Moving this to a separate label reduces indirect branch prediction misses at the cost of a few bytes
SPICY_THREAD_FUNCTION_CALL_PERMANENT_EVENT_HANDLER:
	mov 16(%r15), %rcx
	mov %r15, %rdi
	call *%rcx
	jmp SPICY_THREAD_FUNCTION_LOOP
