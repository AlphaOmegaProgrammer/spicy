#include <inttypes.h> // PRIu32
#include <stdbool.h> // bool / true / false
#include <stdio.h> // sprintf()
#include <stdint.h> // uint32_t
#include <sys/epoll.h> // epoll stuff
#include <threads.h> // thrd_create() / thrd_detach()


#include "SPICY/data-types/core-config.h" // struct SPICY_core_config
#include "SPICY/data-types/threads-config.h" // struct SPICY_threads_config
#include "SPICY/data-types/socket-context.h" // struct SPICY_socket_context

#include "SPICY/threads/thread-function.h" // SPICY_thread_function()


extern struct SPICY_core_config SPICY_core_config;

struct SPICY_threads_config SPICY_threads_config = {
	.thread_pool_size = 1
};

int SPICY_epoll_fd;

bool SPICY_threads_setup(void){
	SPICY_epoll_fd = epoll_create(1);

	if(SPICY_epoll_fd < 0){
		SPICY_core_config.error_message = "Failed to create socket_contexts epoll instance";
		return false;
	}

	thrd_t tid;
	for(uint32_t i = 0; i < SPICY_threads_config.thread_pool_size; i++){
		if(thrd_create(&tid, SPICY_thread_function, NULL) != thrd_success){
			sprintf(SPICY_core_config.error_message, "Failed to create thread #%"PRIu32, i);
			return false;
		}

		if(thrd_detach(tid) != thrd_success){
			sprintf(SPICY_core_config.error_message, "Failed to detach thread #%"PRIu32, i);
			return false;
		}
	}

	return true;
}
