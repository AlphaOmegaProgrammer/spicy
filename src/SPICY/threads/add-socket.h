#ifndef __SPICY_ADD_SOCKET
#define __SPICY_ADD_SOCKET

#include "SPICY/data-types/socket-context.h" // struct SPICY_socket_context

void SPICY_add_socket(int, void(*)(struct SPICY_socket_context*), void*);

#endif
