#ifndef __SPICY_THREADS_EPOLL_SETUP
#define __SPICY_THREADS_EPOLL_SETUP

#include <stdbool.h> // bool

bool SPICY_threads_setup(void);

#endif
