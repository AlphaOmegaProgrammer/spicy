# SPICY

SPICY is an asynchronous and threaded library that provides a blazingly fast event loop focused around network connections.


## Build Requirements

- CMake >= 3.13
- C11 compatible library and compiler
- An x86_64 CPU that supports the CMPXCHG16B instruction (all x86_64 CPUs should)


### Building

Only Linux on the x86_64 architecture is officially supported right now.


#### Build Instructions

1. `cd` into the directory that comes from `git clone https://gitlab.com/AlphaOmegaProgrammer/SPICY.git`
2. Create a new directory `build` and `cd` into it
3. Type `cmake ..` to build the makefiles
4. To compile the SPICY shared library, type `cmake --build .`
5. To install the SPICY shared library, type `cmake --install . --prefix=/usr`


## TO DO

1. Write man pages
2. Verify install works right
