# Set important stuff
cmake_minimum_required(VERSION 3.13)

project(SPICY VERSION 0.1.0.0 LANGUAGES C ASM)


# Include source directories
include_directories(BEFORE SYSTEM src ${CMAKE_CURRENT_BINARY_DIR}/src)
include_directories(BEFORE SYSTEM src ${CMAKE_CURRENT_BINARY_DIR})


# Figure out the ABI if one is not specified
if(NOT DEFINED SPICY_ABI)
	if (${CMAKE_SYSTEM_NAME} MATCHES "^Linux$")
		if (${CMAKE_SYSTEM_PROCESSOR} MATCHES "^x86_64$")
			set(SPICY_ABI "x86_64-sysv-linux" CACHE INTERNAL "")
		endif()
	endif()
endif()

if(NOT DEFINED SPICY_ABI)
	message(FATAL_ERROR "ABI not recognized")
endif()

set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -DSPICY_ABI=${SPICY_ABI}")



# Set some CFLAGS and other things
if(NOT CMAKE_BUILD_TYPE)
	set(CMAKE_BUILD_TYPE RelWithDebInfo)
endif()


string(TOLOWER ${CMAKE_BUILD_TYPE} CMAKE_BUILD_TYPE)

set(DEBUG_BUILD OFF)

if(CMAKE_BUILD_TYPE STREQUAL "debugasan")
	set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -O0 -g3 -ggdb3 -fsanitize=address -fsanitize=undefined -fno-sanitize-recover=all -fsanitize=float-divide-by-zero -fsanitize=float-cast-overflow -fno-sanitize=null -fno-sanitize=alignment")
	set(DEBUG_BUILD ON)

elseif(CMAKE_BUILD_TYPE STREQUAL "debug")
	set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -O0 -g3 -ggdb3")
	set(DEBUG_BUILD ON)

elseif(CMAKE_BUILD_TYPE STREQUAL "minsizerel")
	set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -DNDEBUG -Os")

elseif(CMAKE_BUILD_TYPE STREQUAL "relwithdebinfo")
	set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -g -ggdb -O3")

elseif(CMAKE_BUILD_TYPE STREQUAL "release")
	set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -DNDEBUG -O3")

endif()


if(CMAKE_C_COMPILER_ID STREQUAL "GNU")
	set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -Wall -Wextra -pedantic -Werror")
	set(CMAKE_ASM_FLAGS "${CMAKE_C_FLAGS} ${CMAKE_ASM_FLAGS} -Wa,--warn -Wa,--fatal-warnings")

	if(DEBUG_BUILD EQUAL ON)
		set(CMAKE_ASM_FLAGS "${CMAKE_ASM_FLAGS} -Wa,-g")
	endif()
endif()



if(DEBUG_BUILD)
	set(CMAKE_VERBOSE_MAKEFILE ON)
	set(CMAKE_MESSAGE_LOG_LEVEL "debug")
endif()



message(
	STATUS
	"Build Type: ${CMAKE_BUILD_TYPE}\n"
	"Target ABI: ${SPICY_ABI}\n\n"
)

# Now actually specify how to build the program
add_library(SPICY SHARED)

set_property(TARGET SPICY PROPERTY C_STANDARD 11)
set_property(TARGET SPICY PROPERTY POSITION_INDEPENDENT_CODE ON)

add_subdirectory(src/SPICY)

# Optional testing apps
add_executable(http_server_optimized tests/http-server-optimized.c)
set_property(TARGET http_server_optimized PROPERTY EXCLUDE_FROM_ALL ON)
add_dependencies(http_server_optimized SPICY)
target_link_libraries(http_server_optimized PRIVATE SPICY)

add_executable(open_close tests/open-close.c)
set_property(TARGET open_close PROPERTY EXCLUDE_FROM_ALL ON)
add_dependencies(open_close SPICY)
target_link_libraries(open_close PRIVATE SPICY)



# Install stuff
install(TARGETS SPICY)
