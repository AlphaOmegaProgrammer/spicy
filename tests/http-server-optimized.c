#include <errno.h>
#include <stdint.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <unistd.h>

#include "SPICY/SPICY.h"

char *response = "HTTP/1.1 200 OK\r\ntransfer-encoding: chunked\r\ncontent-type: text/plain\r\n\r\n27\r\nThis is the HTTP demo app response body\r\n0\r\n\r\n";

extern struct SPICY_core_config SPICY_core_config;
extern struct SPICY_threads_config SPICY_threads_config;

void app_handle_event(struct SPICY_socket_context *socket_context){
	do{
		ssize_t write_result = send(socket_context->socket, &response[((uint64_t)socket_context->app_data)], 123 - ((uint64_t)socket_context->app_data), MSG_DONTWAIT);
		if(write_result < 1){
			if(write_result == EAGAIN)
				return;
	
			break;
		}

		socket_context->app_data = (void*)(write_result + ((uint64_t)socket_context->app_data));
	}while(((uint64_t)socket_context->app_data) != 123);

	close(socket_context->socket);
	return;
}


int main(void){
	SPICY_threads_config.thread_pool_size = 3;

	if(!SPICY_init()){
		printf("%s\n", SPICY_core_config.error_message);
		return 1;
	}

	SPICY_start_ipv4_tcp_listener("127.0.0.1", 42069, &SPICY_default_listener_event_handler, &app_handle_event);

	for(;;){
		pause();
	}

	return 0;
}
