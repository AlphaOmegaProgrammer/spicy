#include <stdio.h>
#include <sys/socket.h>
#include <unistd.h>

#include "SPICY/SPICY.h"

extern struct SPICY_core_config SPICY_core_config;
extern struct SPICY_threads_config SPICY_threads_config;

void app_handle_event(struct SPICY_socket_context *socket_context){
	close(socket_context->socket);
}


int main(void){
	SPICY_threads_config.thread_pool_size = 3;

	if(!SPICY_init()){
		printf("%s\n", SPICY_core_config.error_message);
		return 1;
	}

	SPICY_start_ipv4_tcp_listener("127.0.0.1", 42069, &SPICY_default_listener_event_handler, &app_handle_event);

	for(;;){
		pause();
	}

	return 0;
}
